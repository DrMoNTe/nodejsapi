const express = require('express')
const app = express()
const morgan = require('morgan')
const mysql = require('mysql')
const cors = require('cors')
const bodyParser = require('body-parser')
const dateformat = require('dateformat');
function getConnection() {
    return mysql.createConnection({
        host: 'localhost',
        user: 'root',
        //password:'09bae0844ec6e7f30c096a424dfe15512a7c6630a3b8b1e',
        database: 'bara_op'
    })
}


app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static('./public'))
app.use(bodyParser.json());
app.use(morgan('short'))


// MAIN TABLE----------------------

app.post('/main', (req, res) => {
    console.log("Trying to create a new op_data")
    res.set('Content-Type', 'application/json')
    console.log(req.body);

    const c_name = req.body.c_name
    const op_name = req.body.op_name
    const status = req.body.status
    const res_1 = req.body.res_1
    const res_2 = req.body.res_2
    const sale = req.body.sale
    const product = req.body.product
    const start_date = req.body.start_date
    const budget = req.body.budget
    const update_date = req.body.update_date
    const action = req.body.action
    const target_date = req.body.target_date
    const remark = req.body.remark

    const queryString = "INSERT INTO op_table (c_name,op_name,status,res_1,res_2,sale,product,start_date,budget,update_date,action,target_date,remark) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"

    getConnection().query(queryString, [c_name, op_name, status, res_1, res_2, sale, product, start_date, budget, update_date, action, target_date, remark], (err, result, fields) => {
        if (err) {
            console.log("Failed to insert new user: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });

            return
        }
        console.log("Inserted a new with id", result.insertedId);
        res.end()
    })
    res.end()


})

app.put('/main', (req, res) => {
    console.log("Trying to update")
    res.set('Content-Type', 'application/json')
    console.log(req.body);
    const op_id = req.body.op_id
    const c_name = req.body.c_name
    const op_name = req.body.op_name
    const status = req.body.status
    const res_1 = req.body.res_1
    const res_2 = req.body.res_2
    const sale = req.body.sale
    const product = req.body.product
    const start_date = req.body.start_date
    const budget = req.body.budget
    const update_date = req.body.update_date
    const action = req.body.action
    const target_date = req.body.target_date
    const remark = req.body.remark

    const queryString = "UPDATE op_table SET c_name=?,op_name=?,status=?,res_1=?,res_2=?,sale=?,product=?,start_date=?,budget=?,update_date=?,action=?,target_date=?,remark=? WHERE op_id = ?"

    getConnection().query(queryString, [c_name, op_name, status, res_1, res_2, sale, product, start_date, budget, update_date, action, target_date, remark, op_id], (err, result, fields) => {
        if (err) {
            console.log("Failed to update new user: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Updated id", result.updatedId);
        res.end()
    })
    res.end()


})

app.delete('/main/:id', (req, res) => {

    console.log("Deleting Opportunity with id:" + req.params.id)

    const connection = getConnection()

    const opID = req.params.id
    const queryString = "DELETE FROM op_table WHERE op_id = ?"
    connection.query(queryString, [opID], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        res.end()

    })
    res.end()
})
app.get('/main/:id', (req, res) => {
    console.log("Fetching user with id:" + req.params.id)

    const connection = getConnection()

    const userID = req.params.id
    const queryString = "SELECT op_id FROM op_table WHERE op_id = ?"
    connection.query(queryString, [userID], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")

        const op = rows.map((row) => {
            return { firstName: row.first_name, lastName: row.last_name }
        })
        res.json(op)


    })
    //res.end
})


app.get("/main", (req, res) => {
    console.log("Fetching all data")

    const connection = getConnection()

    const queryString = "SELECT * FROM op_table"
    connection.query(queryString, (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });

            return
        }
        console.log("Fetched Successfully")
        const op = rows.map((row) => {
            return {
                op_id: row.op_id,
                c_name: row.c_name,
                op_name: row.op_name,
                status: row.status,
                res_1: row.res_1,
                res_2: row.res_2,
                sale: row.sale,
                product: row.product,
                start_date: dateformat(row.start_date, 'dd/mm/yyyy'),
                budget: row.budget,
                update_date: dateformat(row.update_date, 'dd/mm/yyyy'),
                action: row.action,
                target_date: dateformat(row.target_date, 'dd/mm/yyyy'),
                remark: row.remark

            }
        })
        res.json(op)


    })

})



// CUSTOMER TABLE----------------------------



app.post('/customer', (req, res) => {
    console.log("Trying to create a new customer")
    res.set('Content-Type', 'application/json')
    console.log(req.body);

    const c_name = req.body.c_name
    const c_sname = req.body.c_sname

    const queryString = "INSERT INTO customer (c_name,c_sname) VALUES (?,?)"

    getConnection().query(queryString, [c_name, c_sname], (err, result, fields) => {
        if (err) {
            console.log("Failed to insert customer: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Inserted a new with id", result.insertedId);
        res.end()
    })
    res.end()


})

app.get("/customer", (req, res) => {
    console.log("Fetching all data")

    const connection = getConnection()

    const queryString = "SELECT * FROM customer"
    connection.query(queryString, (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        const data = rows.map((row) => {
            return {
                c_id: row.c_id,
                c_name: row.c_name,
                c_sname: row.c_sname,
            }
        })
        res.json(data)


    })

})
app.get('/customer/:name', (req, res) => {
    console.log("Fetching user with id:" + req.params.name)

    const connection = getConnection()

    const ChkName = req.params.name
    const queryString = "select c_name from customer where c_name = ?"
    connection.query(queryString, [ChkName], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")

        const data = rows.map((row) => {
            return { c_name: row.c_name }
        })
        res.json(data)


    })
    //res.end
})

app.put('/customer', (req, res) => {
    console.log("Trying to update")
    res.set('Content-Type', 'application/json')
    console.log(req.body);
    const c_id = req.body.c_id
    const c_name = req.body.c_name
    const c_sname = req.body.c_sname

    const queryString = "UPDATE customer SET c_name=?,c_sname=? WHERE c_id = ?"

    getConnection().query(queryString, [c_name, c_sname, c_id], (err, result, fields) => {
        if (err) {
            console.log("Failed to update new user: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Updated id", result.updatedId);
        res.end()
    })
    res.end()


})

app.delete('/customer/:id', (req, res) => {

    console.log("Deleting Customer with id:" + req.params.id)

    const connection = getConnection()

    const ID = req.params.id
    const queryString = "DELETE FROM customer WHERE c_id = ?"
    connection.query(queryString, [ID], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        res.end()

    })
    res.end()
})

// OPPORTUNITY TABLE----------------------------

app.post('/opportunity', (req, res) => {
    console.log("Trying to create a new opportunity")
    res.set('Content-Type', 'application/json')
    console.log(req.body);

    const op_name = req.body.op_name

    const queryString = "INSERT INTO opportunity (op_name) VALUES (?)"

    getConnection().query(queryString, [op_name], (err, result, fields) => {
        if (err) {
            console.log("Failed to insert opportunity: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Inserted a new with id", result.insertedId);
        res.end()
    })
    res.end()


})

app.get('/opportunity/:name', (req, res) => {
    console.log("Fetching user with id:" + req.params.name)

    const connection = getConnection()

    const ChkName = req.params.name
    const queryString = "select op_name from opportunity where op_name = ?"
    connection.query(queryString, [ChkName], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")

        const data = rows.map((row) => {
            return { op_name: row.op_name }
        })
        res.json(data)


    })
    //res.end
})

app.get("/opportunity", (req, res) => {
    console.log("Fetching all data")

    const connection = getConnection()

    const queryString = "SELECT * FROM opportunity"
    connection.query(queryString, (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        const data = rows.map((row) => {
            return {
                op_id: row.op_id,
                op_name: row.op_name,
            }
        })
        res.json(data)


    })

})

app.put('/opportunity', (req, res) => {
    console.log("Trying to update")
    res.set('Content-Type', 'application/json')
    console.log(req.body);
    const op_id = req.body.op_id
    const op_name = req.body.op_name
    const queryString = "UPDATE opportunity SET op_name=? WHERE op_id = ?"

    getConnection().query(queryString, [op_name, op_id], (err, result, fields) => {
        if (err) {
            console.log("Failed to update new user: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Updated id", result.updatedId);
        res.end()
    })
    res.end()


})

app.delete('/opportunity/:id', (req, res) => {

    console.log("Deleting Opportunity with id:" + req.params.id)

    const connection = getConnection()

    const ID = req.params.id
    const queryString = "DELETE FROM opportunity WHERE op_id = ?"
    connection.query(queryString, [ID], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        res.end()

    })
    res.end()
})

// PRESALE RESOURCE TABLE----------------------------

app.post('/presale_resource', (req, res) => {
    console.log("Trying to create a new Presale Resource")
    res.set('Content-Type', 'application/json')
    console.log(req.body);

    const ps_name = req.body.ps_name
    const ps_position = req.body.ps_position
    const ps_name2 = req.body.ps_name2

    const queryString = "INSERT INTO presale_resource (ps_name,ps_position,ps_name2) VALUES (?,?,?)"

    getConnection().query(queryString, [ps_name, ps_position, ps_name2], (err, result, fields) => {
        if (err) {
            console.log("Failed to insert presale resource: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Inserted a new with id", result.insertedId);
        res.end()
    })
    res.end()


})

app.get('/presale_resource/:name', (req, res) => {
    console.log("Fetching user with id:" + req.params.name)

    const connection = getConnection()

    const ChkName = req.params.name
    const queryString = "select ps_name from presale_resource where ps_name = ?"
    connection.query(queryString, [ChkName], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")

        const data = rows.map((row) => {
            return { ps_name: row.ps_name }
        })
        res.json(data)


    })
    //res.end
})


app.get("/presale_resource", (req, res) => {
    console.log("Fetching all data")

    const connection = getConnection()

    const queryString = "SELECT * FROM presale_resource"
    connection.query(queryString, (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        const data = rows.map((row) => {
            return {
                ps_id: row.ps_id,
                ps_name: row.ps_name,
                ps_position: row.ps_position,
                ps_name2: row.ps_name2
            }
        })
        res.json(data)


    })

})

app.put('/presale_resource', (req, res) => {
    console.log("Trying to update")
    res.set('Content-Type', 'application/json')
    console.log(req.body);
    const ps_id = req.body.ps_id
    const ps_name = req.body.ps_name
    const ps_position = req.body.ps_position
    const ps_name2 = req.body.ps_name2
    const queryString = "UPDATE presale_resource SET ps_name=? ,ps_position=? ,ps_name2=? WHERE ps_id = ?"

    getConnection().query(queryString, [ps_name, ps_position, ps_name2, ps_id], (err, result, fields) => {
        if (err) {
            console.log("Failed to update new user: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Updated id", result.updatedId);
        res.end()
    })
    res.end()
})

app.delete('/presale_resource/:id', (req, res) => {
    console.log("Deleting Presale Resource with id:" + req.params.id)
    const connection = getConnection()
    const ID = req.params.id
    const queryString = "DELETE FROM presale_resource WHERE ps_id = ?"
    connection.query(queryString, [ID], (err, rows, fields) => {
        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        res.end()
    })
    res.end()
})

// PRODUCT TABLE----------------------------

app.post('/product', (req, res) => {
    console.log("Trying to create a new Product")
    res.set('Content-Type', 'application/json')
    console.log(req.body);
    const p_name = req.body.p_name
    const queryString = "INSERT INTO product (p_name) VALUES (?)"
    getConnection().query(queryString, [p_name], (err, result, fields) => {
        if (err) {
            console.log("Failed to insert opportunity: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Inserted a new with id", result.insertedId);
        res.end()
    })
    res.end()
})
app.get('/product/:name', (req, res) => {
    console.log("Fetching user with id:" + req.params.name)
    const connection = getConnection()
    const ChkName = req.params.name
    const queryString = "select p_name from product where p_name = ?"
    connection.query(queryString, [ChkName], (err, rows, fields) => {
        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")

        const data = rows.map((row) => {
            return { p_name: row.p_name }
        })
        res.json(data)


    })
    //res.end
})

app.get("/product", (req, res) => {
    console.log("Fetching all data")

    const connection = getConnection()

    const queryString = "SELECT * FROM product"
    connection.query(queryString, (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        const data = rows.map((row) => {
            return {
                p_id: row.p_id,
                p_name: row.p_name,
            }
        })
        res.json(data)


    })

})

app.put('/product', (req, res) => {
    console.log("Trying to update")
    res.set('Content-Type', 'application/json')
    console.log(req.body);
    const p_id = req.body.p_id
    const p_name = req.body.p_name
    const queryString = "UPDATE product SET p_name=? WHERE p_id = ?"

    getConnection().query(queryString, [p_name, p_id], (err, result, fields) => {
        if (err) {
            console.log("Failed to update new user: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Updated id", result.updatedId);
        res.end()
    })
    res.end()


})

app.delete('/product/:id', (req, res) => {

    console.log("Deleting Opportunity with id:" + req.params.id)

    const connection = getConnection()

    const ID = req.params.id
    const queryString = "DELETE FROM product WHERE p_id = ?"
    connection.query(queryString, [ID], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        res.end()

    })
    res.end()
})


// SALE TABLE----------------------------

app.post('/sale', (req, res) => {
    console.log("Trying to create a new Sale")
    res.set('Content-Type', 'application/json')
    console.log(req.body);

    const am_name = req.body.am_name
    const am_email = req.body.am_email

    const queryString = "INSERT INTO sale (am_name,am_email) VALUES (?,?)"

    getConnection().query(queryString, [am_name, am_email], (err, result, fields) => {
        if (err) {
            console.log("Failed to insert sale: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Inserted a new with id", result.insertedId);
        res.end()
    })
    res.end()


})

app.get('/sale/:name', (req, res) => {
    console.log("Fetching user with id:" + req.params.name)

    const connection = getConnection()

    const ChkName = req.params.name
    const queryString = "select am_name from sale where am_name = ?"
    connection.query(queryString, [ChkName], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")

        const data = rows.map((row) => {
            return { am_name: row.am_name }
        })
        res.json(data)


    })
    //res.end
})

app.get("/sale", (req, res) => {
    console.log("Fetching all data")

    const connection = getConnection()

    const queryString = "SELECT * FROM sale"
    connection.query(queryString, (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        const data = rows.map((row) => {
            return {
                am_id: row.am_id,
                am_name: row.am_name,
                am_email: row.am_email
            }
        })
        res.json(data)


    })

})

app.put('/sale', (req, res) => {
    console.log("Trying to update")
    res.set('Content-Type', 'application/json')
    console.log(req.body);
    const am_id = req.body.am_id
    const am_name = req.body.am_name
    const am_email = req.body.am_email
    const queryString = "UPDATE sale SET am_name=? ,am_email=? WHERE am_id = ?"

    getConnection().query(queryString, [am_name, am_email, am_id], (err, result, fields) => {
        if (err) {
            console.log("Failed to update new user: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Updated id", result.updatedId);
        res.end()
    })
    res.end()


})

app.delete('/sale/:id', (req, res) => {

    console.log("Deleting Sale with id:" + req.params.id)

    const connection = getConnection()

    const ID = req.params.id
    const queryString = "DELETE FROM sale WHERE am_id = ?"
    connection.query(queryString, [ID], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        res.end()

    })
    res.end()
})

// STATUS TABLE----------------------------

app.post('/status', (req, res) => {
    console.log("Trying to create a new status")
    res.set('Content-Type', 'application/json')
    console.log(req.body);

    const s_name = req.body.s_name

    const queryString = "INSERT INTO status (s_name) VALUES (?)"

    getConnection().query(queryString, [s_name], (err, result, fields) => {
        if (err) {
            console.log("Failed to insert status: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Inserted a new with id", result.insertedId);
        res.end()
    })
    res.end()


})

app.get('/status/:name', (req, res) => {
    console.log("Fetching user with id:" + req.params.name)

    const connection = getConnection()

    const ChkName = req.params.name
    const queryString = "select s_name from status where s_name = ?"
    connection.query(queryString, [ChkName], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")

        const data = rows.map((row) => {
            return { s_name: row.s_name }
        })
        res.json(data)


    })
    //res.end
})

app.get("/status", (req, res) => {
    console.log("Fetching all data")

    const connection = getConnection()

    const queryString = "SELECT * FROM status"
    connection.query(queryString, (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        const data = rows.map((row) => {
            return {
                s_id: row.s_id,
                s_name: row.s_name,
            }
        })
        res.json(data)


    })

})

app.put('/status', (req, res) => {
    console.log("Trying to update")
    res.set('Content-Type', 'application/json')
    console.log(req.body);
    const s_id = req.body.s_id
    const s_name = req.body.s_name
    const queryString = "UPDATE status SET s_name=? WHERE s_id = ?"

    getConnection().query(queryString, [s_name, s_id], (err, result, fields) => {
        if (err) {
            console.log("Failed to update new user: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Updated id", result.updatedId);
        res.end()
    })
    res.end()


})

app.delete('/status/:id', (req, res) => {

    console.log("Deleting Status with id:" + req.params.id)

    const connection = getConnection()

    const ID = req.params.id
    const queryString = "DELETE FROM status WHERE s_id = ?"
    connection.query(queryString, [ID], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        res.end()

    })
    res.end()
})

// TYPE TABLE----------------------------

app.post('/type', (req, res) => {
    console.log("Trying to create a new type")
    res.set('Content-Type', 'application/json')
    console.log(req.body);

    const t_name = req.body.t_name

    const queryString = "INSERT INTO type (t_name) VALUES (?)"

    getConnection().query(queryString, [t_name], (err, result, fields) => {
        if (err) {
            console.log("Failed to insert opportunity: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Inserted a new with id", result.insertedId);
        res.end()
    })
    res.end()


})

app.get('/type/:name', (req, res) => {
    console.log("Fetching user with id:" + req.params.name)

    const connection = getConnection()

    const ChkName = req.params.name
    const queryString = "select t_name from type where t_name = ?"
    connection.query(queryString, [ChkName], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")

        const data = rows.map((row) => {
            return { t_name: row.t_name }
        })
        res.json(data)


    })
    //res.end
})

app.get("/type", (req, res) => {
    console.log("Fetching all data")

    const connection = getConnection()

    const queryString = "SELECT * FROM type"
    connection.query(queryString, (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        const data = rows.map((row) => {
            return {
                t_id: row.t_id,
                t_name: row.t_name,
            }
        })
        res.json(data)


    })

})

app.put('/type', (req, res) => {
    console.log("Trying to update")
    res.set('Content-Type', 'application/json')
    console.log(req.body);
    const t_id = req.body.t_id
    const t_name = req.body.t_name
    const queryString = "UPDATE type SET t_name=? WHERE t_id = ?"

    getConnection().query(queryString, [t_name, t_id], (err, result, fields) => {
        if (err) {
            console.log("Failed to update new user: " + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Updated id", result.updatedId);
        res.end()
    })
    res.end()


})

app.delete('/type/:id', (req, res) => {

    console.log("Deleting Type with id:" + req.params.id)

    const connection = getConnection()

    const ID = req.params.id
    const queryString = "DELETE FROM type WHERE t_id = ?"
    connection.query(queryString, [ID], (err, rows, fields) => {

        if (err) {
            console.log("Failed to query" + err)
            res.status(500).json({
                message: {
                    errno: err.errno,
                    errdecode: err.code,
                    data: err
                }
            });
            return
        }
        console.log("Fetched Successfully")
        res.end()

    })
    res.end()
})


















const PORT = 3003
app.listen(PORT, () => {
    console.log("sadasderver is up and listening on " + PORT)
})

