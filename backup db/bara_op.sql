-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2019 at 11:13 AM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bara_op`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `c_id` varchar(5) NOT NULL DEFAULT '0',
  `c_name` varchar(50) NOT NULL,
  `c_sname` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`c_id`, `c_name`, `c_sname`) VALUES
('C0001', 'PTT GC', 'PTTGC'),
('C0002', 'Star Petroleum', 'SPRC'),
('C0003', 'HomePro', 'HomePro'),
('C0004', 'BFS', 'BFS'),
('C0005', 'Phongchai', 'Phongchai'),
('C0006', 'WHA Group', 'WHA Group'),
('C0007', 'Siam Express Travel Services', 'Siam Express Travel Services'),
('C0008', 'STP&I', 'STP&I'),
('C0009', 'Dole', 'Dole'),
('C0010', 'Big C Vietnam', 'Big C Vietnam'),
('C0011', 'Srichan', 'Srichan'),
('C0012', 'Chatrium', 'Chatrium'),
('C0013', 'BJC', 'BJC'),
('C0014', 'KBANK - Chula', 'KCMH'),
('C0015', 'Department of Fisheries', 'DFMA'),
('C0016', 'Shishedo', 'Shishedo'),
('C0017', 'Kulthorn Kirby', 'KKC'),
('C0018', 'Areeya Property', 'Areeya'),
('C0019', 'Chayo Group', 'Chayo'),
('C0020', 'Reno (Thailand)', NULL),
('C0021', 'Viriyah Insurance', 'Viriyah'),
('C0022', 'DIP', NULL),
('C0023', 'ISC', NULL),
('C0024', 'Gaysorn', NULL),
('C0025', 'Cloud HM', NULL),
('C0026', 'Betagro', NULL),
('C0027', 'Sansiri', NULL),
('C0028', 'AOT', NULL),
('C0029', 'CRG', NULL),
('C0030', 'Zanroo', NULL),
('C0031', 'KTBST', NULL),
('C0032', 'The Nation', NULL),
('C0033', 'SME Bank', NULL),
('C0034', 'Toyota Leasing', NULL),
('C0035', 'Fujiflim', NULL),
('C0036', 'Tesco Lotus', NULL),
('C0037', 'Indara Insurance', NULL),
('C0038', 'UBE Group', NULL),
('C0039', 'PTT Digital', NULL),
('C0040', 'Thairung', NULL),
('C0041', 'The Mall Group', NULL),
('C0042', 'King Power', NULL),
('C0043', 'MK Resturant', NULL),
('C0044', 'Srinanaporn', NULL),
('C0045', 'R.S.', NULL),
('C0046', 'Boonthavorn', NULL),
('C0047', 'PTTAC', NULL),
('C0048', 'Amway', NULL),
('C0049', 'CH.Karnchang', NULL),
('C0050', 'PTTAsahi', NULL),
('C0051', 'RIS', NULL),
('C0052', 'Sino Pacific', NULL),
('C0053', 'AIS', NULL),
('C0054', 'SB Furniture', NULL),
('C0055', 'Siam Makro', NULL),
('C0056', 'RD', NULL),
('C0057', 'CPF_Chester', NULL),
('C0058', 'CPF_FiveStar', NULL),
('C0059', 'asdasdwdw', ''),
('C0060', 'asdasdasdsd', ''),
('C0061', 'asdASD', ''),
('C0062', 'test', ''),
('C0063', 'asdasasd', '');

--
-- Triggers `customer`
--
DELIMITER $$
CREATE TRIGGER `tg_customer_insert` BEFORE INSERT ON `customer` FOR EACH ROW BEGIN
  INSERT INTO customer_seq VALUES (NULL);
  SET NEW.c_id = CONCAT('C', LPAD(LAST_INSERT_ID(), 4, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_seq`
--

CREATE TABLE `customer_seq` (
  `c_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_seq`
--

INSERT INTO `customer_seq` (`c_id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43),
(44),
(45),
(46),
(47),
(48),
(49),
(50),
(51),
(52),
(53),
(54),
(55),
(56),
(57),
(58),
(59),
(60),
(61),
(62),
(63);

-- --------------------------------------------------------

--
-- Table structure for table `opportunity`
--

CREATE TABLE `opportunity` (
  `op_id` varchar(5) NOT NULL DEFAULT '0',
  `op_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opportunity`
--

INSERT INTO `opportunity` (`op_id`, `op_name`) VALUES
('O0001', 'MDM'),
('O0002', 'ESB / API Gateway'),
('O0003', 'System Turn Over Management Phase 1'),
('O0004', 'Manthan Solution'),
('O0005', 'Migrate O365'),
('O0006', 'KACE SMA'),
('O0007', 'Tableau'),
('O0008', 'Solution Architect'),
('O0009', 'One Identity (AD)'),
('O0010', 'M360 ,Windows APT'),
('O0011', 'Chatbot'),
('O0012', 'O365'),
('O0013', 'BI Solution'),
('O0014', 'Retail Analytics'),
('O0015', 'Data Analytics for Food & Beverage'),
('O0016', 'MS SharePoint'),
('O0017', 'Internal Project'),
('O0018', 'Automate Gas Station'),
('O0019', 'Redesign SQL Database for AOT Project'),
('O0020', 'Security update and refreshment'),
('O0021', 'Data Warehouse'),
('O0022', 'Rebuild Office'),
('O0023', 'Face regcognition'),
('O0024', 'DB Migration and performance tuning'),
('O0025', 'Data Modeling'),
('O0026', 'Smart Office'),
('O0027', 'Solution Architect (Oil Station)'),
('O0028', 'Web Scraping'),
('O0029', 'Consult'),
('O0030', 'BPM'),
('O0031', 'Performance Check / Archiving Data (SAP)'),
('O0032', 'Consolidate Data from Logistic'),
('O0033', 'Enable User Tableau / Present other solutions'),
('O0034', 'Plant Preventive Maintenance'),
('O0035', 'Update Product'),
('O0036', 'Datavard'),
('O0037', 'System Turn Over Management Enchancement'),
('O0038', 'Logistics Solution'),
('O0039', 'Robotic Process Automation for Finance'),
('O0040', 'Datavard - Outboard'),
('O0041', 'Digital Transformation'),
('O0042', 'Robotic Process Automation'),
('O0044', 'test');

--
-- Triggers `opportunity`
--
DELIMITER $$
CREATE TRIGGER `tg_opportunity_insert` BEFORE INSERT ON `opportunity` FOR EACH ROW BEGIN
  INSERT INTO opportunity_seq VALUES (NULL);
  SET NEW.op_id = CONCAT('O', LPAD(LAST_INSERT_ID(), 4, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `opportunity_seq`
--

CREATE TABLE `opportunity_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opportunity_seq`
--

INSERT INTO `opportunity_seq` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43),
(44);

-- --------------------------------------------------------

--
-- Table structure for table `op_table`
--

CREATE TABLE `op_table` (
  `op_id` varchar(10) NOT NULL,
  `c_name` varchar(100) DEFAULT NULL,
  `op_name` varchar(255) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `res_1` varchar(100) DEFAULT NULL,
  `res_2` varchar(100) DEFAULT NULL,
  `sale` varchar(100) DEFAULT NULL,
  `product` varchar(100) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `budget` int(11) DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `action` text,
  `target_date` date DEFAULT NULL,
  `remark` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `op_table`
--

INSERT INTO `op_table` (`op_id`, `c_name`, `op_name`, `status`, `res_1`, `res_2`, `sale`, `product`, `start_date`, `budget`, `update_date`, `action`, `target_date`, `remark`) VALUES
('2019010001', 'AOT', 'ESB / API Gateway', 'Budgeting', 'Supakorn', 'Supakorn', 'Nittaya', 'MS O365', '2019-01-09', 0, '2019-05-31', '', '2019-05-31', ''),
('2019010003', 'HomePro', 'Manthan Solution', 'Present', 'Natchapol', 'Narit', 'Sumalee', 'MS O365', '2019-01-15', 0, '2019-05-31', '', '2019-05-31', ''),
('2019010004', 'R.S.', 'System Turn Over Management Phase 1', 'Present', 'Narit', '', 'Preeyawan', 'Manthan', '2019-01-14', 0, '2019-05-31', '', '2019-05-31', ''),
('2019030001', 'Phongchai', 'ESB / API Gateway', 'Budgeting', 'Sonti', '', 'Sumalee', 'MS O365', '2019-03-19', 0, '2019-05-30', '', '2019-05-30', ''),
('2019050001', 'Zanroo', 'System Turn Over Management Phase 1', 'Present', 'PongKhun', 'Natchapol', 'Preeyawan', 'Orchestra', '2019-05-30', 0, '2019-09-12', '', '2019-05-30', ''),
('2019050002', 'HomePro', 'Manthan Solution', 'Budgeting', 'Natchapol', 'Natchapol', 'Nittaya', 'MS O365', '2019-05-12', 0, '2019-05-30', '', '2019-05-30', ''),
('2019050003', 'AOT', 'System Turn Over Management Phase 1', 'POC', 'Natchapol', '', 'Sumalee', 'MS O365', '2019-05-31', 0, '2019-05-31', '', '2019-05-31', ''),
('2019050004', 'Star Petroleum', 'Migrate O365', 'Budgeting', 'Natchapol', '', 'Sumalee', 'Informatica', '2019-05-31', 0, '2019-05-31', '', '2019-05-31', ''),
('2019050005', 'Areeya Property', 'System Turn Over Management Phase 1', 'Present', 'PongKhun', 'Natchapol', 'Preeyawan', 'Orchestra', '2019-05-30', 0, '2019-09-12', '', '2019-05-30', ''),
('2019050006', 'Areeya Property', 'System Turn Over Management Phase 1', 'Present', 'PongKhun', 'Natchapol', 'Preeyawan', 'Orchestra', '2019-05-30', 0, '2019-09-12', '', '2019-05-30', ''),
('2019060001', 'BFS', 'Manthan Solution', 'Budgeting', 'Narit', '', 'Yuwadee', 'Quest', '2019-06-20', 0, '2019-05-30', '', '2019-05-30', ''),
('2019060002', 'BFS', 'System Turn Over Management Phase 1', 'POC', 'Supakorn', 'Supakorn', 'Preeyawan', 'Manthan', '2019-06-04', 0, '2019-06-04', '', '2019-06-04', ''),
('2019060003', 'Star Petroleum', 'ESB / API Gateway', 'Present', 'Narit', 'Narit', 'Yuwadee', 'Orchestra', '2019-06-04', 0, '2019-06-04', '', '2019-06-04', ''),
('2019060004', 'HomePro', 'System Turn Over Management Phase 1', 'Budgeting', 'Narit', 'Natchapol', 'Yuwadee', 'Orchestra', '2019-06-04', 0, '2019-06-04', '', '2019-06-04', ''),
('2019060005', 'Star Petroleum', 'ESB / API Gateway', 'POC', 'Natchapol', 'Supakorn', 'Yuwadee', 'Informatica', '2019-06-04', 0, '2019-06-04', 'ssss', '2019-06-04', ''),
('2019060007', 'Star Petroleum', 'System Turn Over Management Phase 1', 'Budgeting', 'Narit', '', 'Preeyawan', 'MS O365', '2019-06-06', 0, '2019-06-06', '', '2019-06-06', ''),
('2019060008', 'Srichan', 'ESB / API Gateway', 'Present', 'PongKhun', 'Narit', 'Yuwadee', 'Informatica', '2019-06-06', 0, '2019-06-06', 'asdasczxczxc', '2019-06-06', 'asdwdqwd'),
('2019060009', 'Siam Express Travel Services', 'Enable User Tableau / Present other solutions', 'Requirement', 'Narit', 'Natchapol', 'Preeyawan', 'ChatBot - Alan (Better-IT)', '2019-06-06', 2000000, '2019-06-06', 'hello asddwdw lzlzxoc asdlsldlw sdlxccxc ma,msd,masdwlke salskdalksd', '2019-06-06', 'asd,lz lsdal sdwllsad ald'),
('2019060010', 'HomePro', 'System Turn Over Management Phase 1', 'Present', 'Narit', 'Narit', 'Sumalee', 'Informatica', '2019-06-18', 8938499, '2019-06-06', 'asdasdczxczxc', '2019-06-06', 'aasasczxczxczxasaczxcasdczxc'),
('2019060011', 'Star Petroleum', 'System Turn Over Management Phase 1', 'Present', 'Natchapol', 'Natchapol', 'Yuwadee', 'Orchestra', '2019-06-16', 1332212, '2019-06-06', 'lkvdlflkeem,efmlv', '2019-06-06', 'lakalkfelmfwelca'),
('2019060012', 'Star Petroleum', 'Manthan Solution', 'Present', 'Supakorn', '', 'Yuwadee', 'Orchestra', '2019-06-06', 0, '2019-06-06', 'ask-adfg-asd-fas\n-sdf-asd\nf-as\ndf-as\nd-\n-sadfasd-fsadf\n-adsfADSF', '2019-06-06', '-ASDFASDJFAKJBASKDD-SDFASD\n-ASD\nVASD-ADSF\n-ADFSVCV'),
('2019060013', 'BFS', 'System Turn Over Management Phase 1', 'Initial', 'Natchapol', 'Natchapol', 'Yuwadee', 'Orchestra', '2019-06-06', 0, '2019-06-06', '-SFKDKFJSDKJFS;DKFASLKDF\n-ksdjflksdjfalskdjflaskdjfalsdkf', '2019-06-28', 'fajksdflksdjfasdf-sadfas-dfas-df\n-sad\n-fasd\n-fas\nd-fa\ns-df\n\n\naASDsad'),
('2019060014', 'HomePro', 'ESB / API Gateway', 'Present', 'Narit', 'Sonti', 'Sumalee', 'Orchestra', '2019-06-07', 0, '2019-06-07', '', '2019-06-07', ''),
('2019060015', 'AIS', 'BPM', 'Finalized', 'Sonti', 'Natchapol', 'Yuwadee', 'Informatica', '2019-06-07', 0, '2019-06-07', '', '2019-06-07', ''),
('2019060016', 'HomePro', 'MDM', 'Budgeting', 'Supakorn', 'Natchapol', 'Preeyawan', 'Informatica', '2019-06-10', 123123123, '2019-06-10', '', '2019-06-10', ''),
('2019060017', 'HomePro', 'Manthan Solution', 'Present', 'Narit', 'Narit', 'Nittaya', 'Manthan', '2019-06-10', 2147483647, '2019-06-10', '', '2019-06-10', ''),
('2019060018', 'Star Petroleum', 'MDM', 'Initial', 'Sonti', 'Narit', 'Yuwadee', 'Orchestra', '2019-06-10', 0, '2019-06-10', '', '2019-06-10', ''),
('2019060019', 'Star Petroleum', 'ESB / API Gateway', 'Initial', 'Narit', 'Narit', 'Yuwadee', 'Manthan', '2019-06-11', 0, '2019-06-11', '', '2019-06-11', ''),
('2019060020', 'AOT', 'Automate Gas Station', 'Budgeting', 'Narit', 'Supakorn', 'Kanyarat', 'Datavard Fitness Test', '2019-06-11', 111, '2019-06-11', 'asdasdasda\nsd\nas\ndas\nda\ns\nasda\nda\nsda\nsd', '2019-06-11', 'asdas\ndas\n\nzxc\nzxc\nzx\nc'),
('2019060022', 'Department of Fisheries', 'BI Solution', 'Closed', 'Narit', 'Sonti', 'Sumalee', 'Datavard Fitness Test', '2019-06-11', 0, '2019-06-19', '', '2019-06-11', ''),
('2019060024', 'Dole', 'BI Solution', 'Initial', 'Natchapol', 'Natchapol', 'Kanyarat', 'Epicor (ERP)', '2019-06-11', 0, '2019-06-11', '', '2019-06-11', ''),
('2019060025', 'Dole', 'Performance Check / Archiving Data (SAP)', 'Closed', 'PongKhun', 'Sonti', 'Kanyarat', 'Datavard Fitness Test', '2019-06-11', 0, '2019-06-11', '', '2019-06-11', ''),
('2019060026', 'Amway', 'Enable User Tableau / Present other solutions', 'Initial', 'PongKhun', 'Sonti', 'Nittaya', 'Manthan', '2019-06-12', 0, '2019-06-12', '', '2019-06-12', ''),
('2019060027', 'asdasdasdsd', 'BI Solution', 'Initial', 'PongKhun', NULL, 'Kanyarat', 'ChatBot', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060028', 'SB Furniture', 'Automate Gas Station', 'Budgeting', 'Narit', '', 'Kanchaya', 'ChatBot', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060030', 'AOT', 'BPM', 'Finalized', 'Supakorn', 'Supakorn', 'Sumalee', 'Datavard Fitness Test', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060031', 'AOT', 'DB Migration and performance tuning', 'Initial', 'Natchapol', 'Sonti', 'Kanchaya', 'ChatBot', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060032', 'HomePro', 'Datavard', 'Budgeting', 'Narit', 'PongKhun', 'Kanchaya', 'Datavard Fitness Test', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060033', 'asdasdasdsd', 'Automate Gas Station', 'Budgeting', 'Narit', '', 'Kanchaya', 'ChatBot', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060034', 'AIS', 'Automate Gas Station', 'Budgeting', 'Narit', 'Narit', 'Kanchaya', 'ChatBot - Alan (Better-IT)', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060035', 'asdASD', 'Automate Gas Station', 'Budgeting', 'Narit', 'Narit', 'Kanchaya', 'ChatBot', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060036', 'Sansiri', 'BPM', 'Closed', 'Natchapol', NULL, 'Preeyawan', 'Epicor (ERP)', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060037', 'AOT', 'Chatbot', 'Closed', 'Sonti', '', 'Nittaya', 'RPA/ChatBot', '2019-06-22', 0, '2019-06-02', 'asdasdccx', '2019-06-01', 'asdasdxcc'),
('2019060038', 'AIS', 'Automate Gas Station', 'Budgeting', 'Narit', 'Narit', 'Kanchaya', 'ChatBot', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060039', 'AIS', 'Automate Gas Station', 'Budgeting', 'Narit', 'Narit', 'Kanchaya', 'ChatBot', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060040', 'AIS', 'Automate Gas Station', 'Budgeting', 'Narit', 'Narit', 'Kanchaya', 'ChatBot', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060041', 'Areeya Property', 'Chatbot', 'Lost', 'Sonti', NULL, 'Sumalee', 'Epicor (ERP)', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060042', 'Areeya Property', 'BPM', 'Finalized', 'Natchapol', '', 'Nittaya', 'Datavard Fitness Test', '2019-06-13', 0, '2019-06-13', '', '2019-06-13', ''),
('2019060043', 'SB Furniture', 'Data Analytics for Food & Beverage', 'Closed', 'PongKhun', '', 'Preeyawan', 'ChatBot - Alan (Better-IT)', '2019-06-13', 99999, '2019-06-21', '', '2019-06-13', '');

--
-- Triggers `op_table`
--
DELIMITER $$
CREATE TRIGGER `tg_op_table_delete` AFTER DELETE ON `op_table` FOR EACH ROW begin 
	IF (SELECT DATE_FORMAT((old.start_date),'%m')=01) THEN
   DELETE FROM op_table_seq201901 where id = (select RIGHT(old.op_id,4));
   	ELSEIF (SELECT DATE_FORMAT((old.start_date),'%m')=02) THEN
   DELETE FROM op_table_seq201902 where id = (select RIGHT(old.op_id,4));
      	ELSEIF (SELECT DATE_FORMAT((old.start_date),'%m')=03) THEN
   DELETE FROM op_table_seq201903 where id = (select RIGHT(old.op_id,4));
      	ELSEIF (SELECT DATE_FORMAT((old.start_date),'%m')=04) THEN
   DELETE FROM op_table_seq201904 where id = (select RIGHT(old.op_id,4));
    	ELSEIF (SELECT DATE_FORMAT((old.start_date),'%m')=05) THEN
   DELETE FROM op_table_seq201905 where id = (select RIGHT(old.op_id,4));
      	ELSEIF (SELECT DATE_FORMAT((old.start_date),'%m')=06) THEN
   DELETE FROM op_table_seq201906 where id = (select RIGHT(old.op_id,4));
      	ELSEIF (SELECT DATE_FORMAT((old.start_date),'%m')=07) THEN
   DELETE FROM op_table_seq201907 where id = (select RIGHT(old.op_id,4));
      	ELSEIF (SELECT DATE_FORMAT((old.start_date),'%m')=08) THEN
   DELETE FROM op_table_seq201908 where id = (select RIGHT(old.op_id,4));
      	ELSEIF (SELECT DATE_FORMAT((old.start_date),'%m')=09) THEN
   DELETE FROM op_table_seq201909 where id = (select RIGHT(old.op_id,4));
      	ELSEIF (SELECT DATE_FORMAT((old.start_date),'%m')=10) THEN
   DELETE FROM op_table_seq201910 where id = (select RIGHT(old.op_id,4));
      	ELSEIF (SELECT DATE_FORMAT((old.start_date),'%m')=11) THEN
   DELETE FROM op_table_seq201911 where id = (select RIGHT(old.op_id,4));
      	ELSEIF (SELECT DATE_FORMAT((old.start_date),'%m')=12) THEN
   DELETE FROM op_table_seq201912 where id = (select RIGHT(old.op_id,4));
  END IF;
  END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `tg_op_table_insert` BEFORE INSERT ON `op_table` FOR EACH ROW BEGIN
IF (SELECT DATE_FORMAT((NEW.start_date),'%m')=01) THEN
  INSERT INTO op_table_seq201901 VALUES (NULL);
  SET NEW.op_id = CONCAT((SELECT DATE_FORMAT((NEW.start_date),'%Y%m')), LPAD(LAST_INSERT_ID(), 4, '0'));
  ELSEIF (SELECT DATE_FORMAT((NEW.start_date),'%m')=02) THEN
  INSERT INTO op_table_seq201902 VALUES (NULL);
  SET NEW.op_id = CONCAT((SELECT DATE_FORMAT((NEW.start_date),'%Y%m')), LPAD(LAST_INSERT_ID(), 4, '0'));
    ELSEIF (SELECT DATE_FORMAT((NEW.start_date),'%m')=03) THEN
  INSERT INTO op_table_seq201903 VALUES (NULL);
  SET NEW.op_id = CONCAT((SELECT DATE_FORMAT((NEW.start_date),'%Y%m')), LPAD(LAST_INSERT_ID(), 4, '0'));
    ELSEIF (SELECT DATE_FORMAT((NEW.start_date),'%m')=04) THEN
  INSERT INTO op_table_seq201904 VALUES (NULL);
  SET NEW.op_id = CONCAT((SELECT DATE_FORMAT((NEW.start_date),'%Y%m')), LPAD(LAST_INSERT_ID(), 4, '0'));
    ELSEIF (SELECT DATE_FORMAT((NEW.start_date),'%m')=05) THEN
  INSERT INTO op_table_seq201905 VALUES (NULL);
  SET NEW.op_id = CONCAT((SELECT DATE_FORMAT((NEW.start_date),'%Y%m')), LPAD(LAST_INSERT_ID(), 4, '0'));
    ELSEIF (SELECT DATE_FORMAT((NEW.start_date),'%m')=06) THEN
  INSERT INTO op_table_seq201906 VALUES (NULL);
  SET NEW.op_id = CONCAT((SELECT DATE_FORMAT((NEW.start_date),'%Y%m')), LPAD(LAST_INSERT_ID(), 4, '0'));
    ELSEIF (SELECT DATE_FORMAT((NEW.start_date),'%m')=07) THEN
  INSERT INTO op_table_seq201907 VALUES (NULL);
  SET NEW.op_id = CONCAT((SELECT DATE_FORMAT((NEW.start_date),'%Y%m')), LPAD(LAST_INSERT_ID(), 4, '0'));
    ELSEIF (SELECT DATE_FORMAT((NEW.start_date),'%m')=08) THEN
  INSERT INTO op_table_seq201908 VALUES (NULL);
  SET NEW.op_id = CONCAT((SELECT DATE_FORMAT((NEW.start_date),'%Y%m')), LPAD(LAST_INSERT_ID(), 4, '0'));
    ELSEIF (SELECT DATE_FORMAT((NEW.start_date),'%m')=09) THEN
  INSERT INTO op_table_seq201909 VALUES (NULL);
  SET NEW.op_id = CONCAT((SELECT DATE_FORMAT((NEW.start_date),'%Y%m')), LPAD(LAST_INSERT_ID(), 4, '0'));
    ELSEIF (SELECT DATE_FORMAT((NEW.start_date),'%m')=10) THEN
  INSERT INTO op_table_seq201910 VALUES (NULL);
  SET NEW.op_id = CONCAT((SELECT DATE_FORMAT((NEW.start_date),'%Y%m')), LPAD(LAST_INSERT_ID(), 4, '0'));
    ELSEIF (SELECT DATE_FORMAT((NEW.start_date),'%m')=11) THEN
  INSERT INTO op_table_seq201911 VALUES (NULL);
  SET NEW.op_id = CONCAT((SELECT DATE_FORMAT((NEW.start_date),'%Y%m')), LPAD(LAST_INSERT_ID(), 4, '0'));
    ELSEIF (SELECT DATE_FORMAT((NEW.start_date),'%m')=12) THEN
  INSERT INTO op_table_seq201912 VALUES (NULL);
  SET NEW.op_id = CONCAT((SELECT DATE_FORMAT((NEW.start_date),'%Y%m')), LPAD(LAST_INSERT_ID(), 4, '0'));
  END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `op_table_seq201901`
--

CREATE TABLE `op_table_seq201901` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `op_table_seq201901`
--

INSERT INTO `op_table_seq201901` (`id`) VALUES
(1),
(3),
(4);

-- --------------------------------------------------------

--
-- Table structure for table `op_table_seq201902`
--

CREATE TABLE `op_table_seq201902` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `op_table_seq201903`
--

CREATE TABLE `op_table_seq201903` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `op_table_seq201903`
--

INSERT INTO `op_table_seq201903` (`id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `op_table_seq201904`
--

CREATE TABLE `op_table_seq201904` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `op_table_seq201905`
--

CREATE TABLE `op_table_seq201905` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `op_table_seq201905`
--

INSERT INTO `op_table_seq201905` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6);

-- --------------------------------------------------------

--
-- Table structure for table `op_table_seq201906`
--

CREATE TABLE `op_table_seq201906` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `op_table_seq201906`
--

INSERT INTO `op_table_seq201906` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(22),
(24),
(25),
(26),
(27),
(28),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43);

-- --------------------------------------------------------

--
-- Table structure for table `op_table_seq201907`
--

CREATE TABLE `op_table_seq201907` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `op_table_seq201908`
--

CREATE TABLE `op_table_seq201908` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `op_table_seq201909`
--

CREATE TABLE `op_table_seq201909` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `op_table_seq201910`
--

CREATE TABLE `op_table_seq201910` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `op_table_seq201911`
--

CREATE TABLE `op_table_seq201911` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `op_table_seq201912`
--

CREATE TABLE `op_table_seq201912` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `presale_resource`
--

CREATE TABLE `presale_resource` (
  `ps_id` varchar(6) NOT NULL DEFAULT '0',
  `ps_name` varchar(50) DEFAULT NULL,
  `ps_position` varchar(50) DEFAULT NULL,
  `ps_name2` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `presale_resource`
--

INSERT INTO `presale_resource` (`ps_id`, `ps_name`, `ps_position`, `ps_name2`) VALUES
('PS0001', 'Sonti', 'Enterprise Architect Consultant', 'Sonti'),
('PS0002', 'Narit', 'Solution Consultant', 'Narit'),
('PS0003', 'Natchapol', 'Solution Consultant', 'Natchapol'),
('PS0004', 'Supakorn', 'Developer', 'Supakorn'),
('PS0005', 'PongKhun', 'Presale', 'Pongkhun'),
('PS0008', 'test', 'test', 'test');

--
-- Triggers `presale_resource`
--
DELIMITER $$
CREATE TRIGGER `tg_presale_resource_insert` BEFORE INSERT ON `presale_resource` FOR EACH ROW BEGIN
  INSERT INTO presale_resource_seq VALUES (NULL);
  SET NEW.ps_id = CONCAT('PS', LPAD(LAST_INSERT_ID(), 4, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `presale_resource_seq`
--

CREATE TABLE `presale_resource_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `presale_resource_seq`
--

INSERT INTO `presale_resource_seq` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `p_id` varchar(5) NOT NULL DEFAULT '0',
  `p_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`p_id`, `p_name`) VALUES
('P0001', 'Informatica'),
('P0002', 'Orchestra'),
('P0003', 'Manthan'),
('P0004', 'MS O365'),
('P0005', 'Quest'),
('P0006', 'Tableau'),
('P0007', 'Microsoft'),
('P0008', 'Rapid Miner'),
('P0009', 'Epicor (ERP)'),
('P0010', 'K2'),
('P0011', 'Datavard Fitness Test'),
('P0012', 'ChatBot - Alan (Better-IT)'),
('P0013', 'Kratos'),
('P0014', 'IBM RPA'),
('P0015', 'ChatBot'),
('P0016', 'RPA/ChatBot'),
('P0018', 'test');

--
-- Triggers `product`
--
DELIMITER $$
CREATE TRIGGER `tg_product_insert` BEFORE INSERT ON `product` FOR EACH ROW BEGIN
  INSERT INTO product_seq VALUES (NULL);
  SET NEW.p_id = CONCAT('P', LPAD(LAST_INSERT_ID(), 4, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `product_seq`
--

CREATE TABLE `product_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_seq`
--

INSERT INTO `product_seq` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18);

-- --------------------------------------------------------

--
-- Table structure for table `sale`
--

CREATE TABLE `sale` (
  `am_id` varchar(6) NOT NULL DEFAULT '0',
  `am_name` varchar(50) DEFAULT NULL,
  `am_email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sale`
--

INSERT INTO `sale` (`am_id`, `am_name`, `am_email`) VALUES
('AM0001', 'Sumalee', 'sumalee@baraadv.co.th'),
('AM0002', 'Yuwadee', 'yuwadee@baraadv.co.th'),
('AM0003', 'Preeyawan', 'preeyawan@baraadv.co.th'),
('AM0004', 'Nittaya', 'nittaya@baraadv.co.th'),
('AM0005', 'Kanyarat', 'kanyarat@baraadv.co.th'),
('AM0006', 'Kanchaya', 'kanchaya.p@baraadv.co.th'),
('AM0009', 'test', 'test');

--
-- Triggers `sale`
--
DELIMITER $$
CREATE TRIGGER `tg_sale_insert` BEFORE INSERT ON `sale` FOR EACH ROW BEGIN
  INSERT INTO sale_seq VALUES (NULL);
  SET NEW.am_id = CONCAT('AM', LPAD(LAST_INSERT_ID(), 4, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sale_seq`
--

CREATE TABLE `sale_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sale_seq`
--

INSERT INTO `sale_seq` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `s_id` varchar(5) NOT NULL DEFAULT '0',
  `s_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`s_id`, `s_name`) VALUES
('S0001', 'Initial'),
('S0002', 'Present'),
('S0003', 'POC'),
('S0004', 'Budgeting'),
('S0005', 'Finalized'),
('S0006', 'UAT'),
('S0007', 'Pending'),
('S0008', 'Closed'),
('S0009', 'Requirement'),
('S0010', 'Won'),
('S0011', 'Upside'),
('S0012', 'Lost'),
('S0014', 'test');

--
-- Triggers `status`
--
DELIMITER $$
CREATE TRIGGER `tg_status_insert` BEFORE INSERT ON `status` FOR EACH ROW BEGIN
  INSERT INTO status_seq VALUES (NULL);
  SET NEW.s_id = CONCAT('S', LPAD(LAST_INSERT_ID(), 4, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `status_seq`
--

CREATE TABLE `status_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_seq`
--

INSERT INTO `status_seq` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14);

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `t_id` varchar(5) NOT NULL DEFAULT '0',
  `t_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`t_id`, `t_name`) VALUES
('T0001', 'Presale Presentation'),
('T0002', 'Internal Project'),
('T0003', 'Consulting'),
('T0004', 'Imprementation'),
('T0005', 'Customization'),
('T0006', 'Training'),
('T0007', 'Documentation'),
('T0008', 'Devolop'),
('T0009', 'Training'),
('T0010', 'PS & Training'),
('T0011', 'SW'),
('T0012', 'HW');

--
-- Triggers `type`
--
DELIMITER $$
CREATE TRIGGER `tg_type_insert` BEFORE INSERT ON `type` FOR EACH ROW BEGIN
  INSERT INTO type_seq VALUES (NULL);
  SET NEW.t_id = CONCAT('T', LPAD(LAST_INSERT_ID(), 4, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `type_seq`
--

CREATE TABLE `type_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_seq`
--

INSERT INTO `type_seq` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `customer_seq`
--
ALTER TABLE `customer_seq`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `opportunity`
--
ALTER TABLE `opportunity`
  ADD PRIMARY KEY (`op_id`);

--
-- Indexes for table `opportunity_seq`
--
ALTER TABLE `opportunity_seq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `op_table`
--
ALTER TABLE `op_table`
  ADD PRIMARY KEY (`op_id`);

--
-- Indexes for table `op_table_seq201901`
--
ALTER TABLE `op_table_seq201901`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `op_table_seq201902`
--
ALTER TABLE `op_table_seq201902`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `op_table_seq201903`
--
ALTER TABLE `op_table_seq201903`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `op_table_seq201904`
--
ALTER TABLE `op_table_seq201904`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `op_table_seq201905`
--
ALTER TABLE `op_table_seq201905`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `op_table_seq201906`
--
ALTER TABLE `op_table_seq201906`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `op_table_seq201907`
--
ALTER TABLE `op_table_seq201907`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `op_table_seq201908`
--
ALTER TABLE `op_table_seq201908`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `op_table_seq201909`
--
ALTER TABLE `op_table_seq201909`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `op_table_seq201910`
--
ALTER TABLE `op_table_seq201910`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `op_table_seq201911`
--
ALTER TABLE `op_table_seq201911`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `op_table_seq201912`
--
ALTER TABLE `op_table_seq201912`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `presale_resource`
--
ALTER TABLE `presale_resource`
  ADD PRIMARY KEY (`ps_id`);

--
-- Indexes for table `presale_resource_seq`
--
ALTER TABLE `presale_resource_seq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `product_seq`
--
ALTER TABLE `product_seq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`am_id`);

--
-- Indexes for table `sale_seq`
--
ALTER TABLE `sale_seq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `status_seq`
--
ALTER TABLE `status_seq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`t_id`);

--
-- Indexes for table `type_seq`
--
ALTER TABLE `type_seq`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer_seq`
--
ALTER TABLE `customer_seq`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `opportunity_seq`
--
ALTER TABLE `opportunity_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `op_table_seq201901`
--
ALTER TABLE `op_table_seq201901`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `op_table_seq201902`
--
ALTER TABLE `op_table_seq201902`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `op_table_seq201903`
--
ALTER TABLE `op_table_seq201903`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `op_table_seq201904`
--
ALTER TABLE `op_table_seq201904`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `op_table_seq201905`
--
ALTER TABLE `op_table_seq201905`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `op_table_seq201906`
--
ALTER TABLE `op_table_seq201906`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `op_table_seq201907`
--
ALTER TABLE `op_table_seq201907`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `op_table_seq201908`
--
ALTER TABLE `op_table_seq201908`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `op_table_seq201909`
--
ALTER TABLE `op_table_seq201909`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `op_table_seq201910`
--
ALTER TABLE `op_table_seq201910`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `op_table_seq201911`
--
ALTER TABLE `op_table_seq201911`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `op_table_seq201912`
--
ALTER TABLE `op_table_seq201912`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `presale_resource_seq`
--
ALTER TABLE `presale_resource_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product_seq`
--
ALTER TABLE `product_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sale_seq`
--
ALTER TABLE `sale_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `status_seq`
--
ALTER TABLE `status_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `type_seq`
--
ALTER TABLE `type_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
